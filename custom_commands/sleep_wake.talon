mode: all
-
^morning$:
    user.mouse_toggle_control_mouse()
    user.talon_mode()
^drowsy [<phrase>]$:
    user.switcher_hide_running()
    user.history_disable()
    user.homophones_hide()
    user.help_hide()
    user.mouse_toggle_control_mouse()
    speech.disable()
    user.engine_sleep()

