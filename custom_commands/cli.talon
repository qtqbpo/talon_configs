# app: wezterm
-
pacman update: insert("sudo pacman -Syu")
pacman install: insert("sudo pacman -S ")
yay update: insert("yay -Syua")
yay install: insert("yay -S ")
yay uninstall: insert("yay -Rs ")
flatpak update: insert("flatpak update")
flatpak uninstall: insert("flatpak remove ")

git name: insert("git config user.name ")
git email: insert("git config user.email ")