app: element
-
bold this: key(ctrl-b)
italic this: key(ctrl-i)
quote this: key(ctrl->)

newline: key(shift-enter)

toggle mic: key(ctrl-d)
toggle cam: key(ctrl-e)

go unread: key(shift-pageup)

room search: key(ctrl-k)