os: linux
-
tag(): user.mouse_cursor_commands_enable

show active: key(super)
show apps: key(super-a)
show right: key(ctrl-alt-right)
show left: key(ctrl-alt-left)
show note: key(super-v)
window max: key(super-up)
move right: key(ctrl-shift-alt-right)
move left: key(ctrl-shift-alt-left)