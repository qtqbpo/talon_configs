app: wezterm
-
paste that:key(ctrl-shift-v)
copy that:key(ctrl-shift-c)
#clear word: key(leftalt-bksp)

pacman update: user.paste("sudo pacman -Syu")
pacman install:
    insert("sudo pacman -S")
    key(space)
pacman remove:
        insert("sudo pacman -Rs")
        key(space)
yay update: insert("yay -Syua")
yay install:
    insert("yay -S")
    key(space)
flatpak update: insert("flatpak update")