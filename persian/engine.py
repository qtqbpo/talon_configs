from talon import speech_system, Context
from talon.engines.vosk import VoskEngine

vosk_fa = VoskEngine(model='vosk-model-small-fa-0.5', language='fa_FA')
speech_system.add_engine(vosk_fa)