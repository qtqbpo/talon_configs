mode: command
-
^persian mode$:
	mode.disable("command")
	mode.enable("user.persian")