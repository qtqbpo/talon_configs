import talon
from talon import Context, Module


mod = Module()
mod.mode('persian')

ctx = Context()
ctx.matches = 'mode: user.persian'
ctx.settings = {
	'speech.engine': 'vosk',
	'speech.timeout': 0.3
}

mod.list("alephba", desc="Persian phonetic alphabet")
ctx.lists["self.alephba"] = {
	"آب" : "ا",
	"باد" : "ب",
	"پاس" : "پ",
	"توت" : "ت",
	"کثیف" : "ث",
	"جا" : "ج",
	"چاه" : "چ",
	"حرف" : "ح",
	"داد" : "د",
	"ذلیل" : "ذ",
	"راه" : "ر",
	"زمین" : "ز",
	"ژاله" : "ژ",
	"سیب" : "س",
	"شاه" : "ش",
	"صابون" : "ص",
	"ضرر" : "ض",
	"طریق" : "ط",
	"ظلم" : "ظ",
	"علم" : "ع",
	"غم" : "غ",
	"فربه" : "ف",
	"قصر" : "ق",
	"کار" : "ک",
	"گشت" : "گ",
	"لمس" : "ل",
	"مرد" : "م",
	"نسیم" : "ن",
	"وسط" : "و",
	"هوس" : "ه",
	"یک" : "ی",
}

@mod.capture(rule='({self.alephba}+)')
def alephba(m) -> str:
	return str(m)
