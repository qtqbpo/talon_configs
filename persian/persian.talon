mode: user.persian
language: fa_FA

-

^حالت فرمان$:
    mode.disable("user.persian")
	mode.enable("command")

^<user.alephba>$: user.paste(user.alephba)

فاصله: key(space)
نیم فاصله: key(shift-space)
نقطه: user.paste(".")
سه نقطه: user.paste("...")
ویرگول: user.paste("،")
علامت سوال: user.paste("؟")
علامت تعجب: user.paste("!")

پاک: key(bksp)
خط جدید: key(enter)
برگشت: key(ctrl-z)

بگو <phrase>: user.paste(phrase)